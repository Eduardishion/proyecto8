import React, { Component } from 'react'

export default class Error extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.mensaje}</h1>
      </div>
    )
  }
}
