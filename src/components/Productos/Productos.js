import React, { Component } from 'react'
import Producto from '../Producto/Producto'
import './Productos.css'
import Buscador from '../Buscador/Buscador'

export default class Productos extends Component {
  render() {
    return (
      <div className="productos">
        <h2>Nuestros Productos</h2>
        <Buscador busquedaProducto={this.props.busquedaProducto} />
        <ul className="lista-productos">
          {Object.keys(this.props.productos).map(key => (
            <Producto key={key} informacion={this.props.productos[key]} />
          ))}
        </ul>
      </div>
    )
  }
}
