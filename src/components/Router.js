import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import datos from '../../assets/datos.json'

import Productos from './Productos/Productos'
import Nosotros from './Nosotros/Nosotros'
import Error from './Error/Error'
import Header from './Header/Header'
import SingleProducto from './SingleProducto/SingleProducto'
import Navegacion from './Navegacion/Navegacion'
import Contacto from './Contacto/Contacto'

export default class Router extends Component {
  constructor(props) {
    super(props)
    this.state = {
      productos: datos,
      terminoBusqueda: ''
    }
  }

  busquedaProducto = busqueda => {
    console.log(busqueda)

    busqueda.length > 3
      ? this.setState({
          terminoBusqueda: busqueda
        })
      : this.setState({
          terminoBusqueda: ''
        })
  }

  render() {
    let productos = [...this.state.productos]
    let busqueda = this.state.terminoBusqueda
    let resultado =
      busqueda !== ''
        ? productos.filter(
            producto =>
              producto.nombre.toLowerCase().indexOf(busqueda.toLowerCase()) !==
              -1
          )
        : productos

    return (
      <BrowserRouter>
        <div className="contenedor">
          <Header />
          <Navegacion />
          <Switch>
            <Route
              exact
              path="/"
              render={() => (
                <Productos
                  productos={resultado}
                  busquedaProducto={this.busquedaProducto}
                />
              )}
            />
            <Route
              exact
              path="/productos"
              render={() => (
                <Productos
                  productos={resultado}
                  busquedaProducto={this.busquedaProducto}
                />
              )}
            />
            <Route exact path="/nosotros" component={Nosotros} />
            <Route exact path="/contacto" component={Contacto} />
            <Route
              exact
              path="/producto/:id"
              render={props => {
                let idProductos = props.match.params.id
                return (
                  <SingleProducto
                    producto={this.state.productos[idProductos]}
                  />
                )
              }}
            />
            <Route render={() => <Error mensaje="Ruta no encontrada" />} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}
