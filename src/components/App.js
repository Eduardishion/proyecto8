import React, { Component } from 'react'
import '../css/App.css'

import Router from './Router'

export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router />
      </React.Fragment>
    )
  }
}
