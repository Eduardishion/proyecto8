import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Producto extends Component {
  render() {
    const { imagen, nombre, precio, id } = this.props.informacion

    return (
      <li>
        <img src={`../../assets/img/${imagen}.png`} alt={nombre} />
        <p>
          {nombre} <span>${precio}</span>
        </p>
        <Link to={`/producto/${id}`}>Más información</Link>
      </li>
    )
  }
}
